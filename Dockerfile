# Use an official Python runtime as an image
FROM python:3.5-alpine

MAINTAINER TAYARI Oussama

WORKDIR /app

# Install any needed packages specified in requirements.txt
COPY source_code /app
RUN pip install -r requirements.txt

# Run server.py when the container launches
EXPOSE 8181
#CMD python server.py
ENTRYPOINT ["python", "server.py"]
